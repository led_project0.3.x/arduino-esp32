## Library 설치

아두이노의 CPU에 모여있는 곳을 보면 esp32폴더가 있는데 기존에 설치 된 2.0.1 버전에 대한 폴더가 있을 것이다. 

2.0.1 폴더가 있으려면 기존에 esp32 2.0.1을 받아야 한다.

![Board Manager](img/board_mg.png)

한글을 처리하기 위해서는 라이브러를 설치 후에 esp32폴더 위치에 가서  2.0.1을 제거 후 다시 아래 위치에서 새로 받아야 한다. (순서를 꼭 지켜야 한다.)

- [ ]  esp32 폴더 위치

cd C:\Users\FAMILY\AppData\Local\Arduino15\packages\esp32\hardware\esp32\

- [ ]  2.0.1 받을 위치
- git clone [https://gitlab.com/led_project0.3.x/arduino-esp32](https://gitlab.com/led_project0.3.x/arduino-esp32) 2.0.1